/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */

/* Copyright 2015 Sanjiban Choudhury
 * scratch.cpp
 *
 *  Created on: Mar 4, 2016
 *      Author: Sanjiban Choudhury
 */

#include <iostream>
#include "polynomials/polynomial_fit.h"
#include "polynomials/polynomial_utils.h"

using namespace ca;

namespace pf = polynomial_fit;
namespace pu = polynomial_utils;

void test_lsq_4deg() {
	pf::PolyfitConstraints constraints;
	constraints.C0_bv.first = 0;
	constraints.C0_bv.second = 0.003343891622696;
	constraints.C1_bv.first = 0;
	constraints.C1_bv.second = 0;
	constraints.limit = 0.003343891622696;
	constraints.limit_dot = 4.525434009001028e-05;
	constraints.limit_ddot = 1.790421383893963e-06;

	Eigen::VectorXd lb(5), ub(5);
	lb << -0.001, -5, -5, -5, -5;
	ub << 0.001, 5, 5, 5, 5;

	bool flag;
	Polynomial<double, 4> p = Fit4DPolyC1_lsq(flag, constraints, 120., lb, ub);

	int res = flag ? 1 : 0;
	std::cout << "Polynomial coeffs: " << p << "\n";
	std::cout << "Result: " << res << "\n";
}

int main(int argc, char **argv) {
//  Polynomial<double, 3> poly = pf::GetCubicPoly(-1, 4, Eigen::Vector2d(3, -6), Eigen::Vector2d(2, 5));
//  std::cout << poly;
//  std::cout << "Value at -1: " << poly.Evaluate(-1) << " Value at 4: " << poly.Evaluate(4) << std::endl;
//  std::cout << "Min Value of pos: " << pu::GetMinValue(poly) << std::endl;
//  std::cout << "Max Value of pos: " << pu::GetMaxValue(poly) << std::endl;
//  std::cout << "Abs max Value of pos: " << pu::GetMaxAbsValue(poly) << std::endl;
//
//
//  Polynomial<double, 2> poly_d = poly.Derivative();
//  std::cout << poly_d;
//  std::cout << "Der Value at -1: " << poly_d.Evaluate(-1) << " Der Value at 4: " << poly_d.Evaluate(4) << std::endl;
//  std::cout << "Min Value of vel: " << pu::GetMinValue(poly_d) << std::endl;
//  std::cout << "Max Value of vel: " << pu::GetMaxValue(poly_d) << std::endl;
//  std::cout << "Abs max Value of vel: " << pu::GetMaxAbsValue(poly_d) << std::endl;
//
//  Polynomial<double, 3> feas_poly = pf::GetFeasInterpCubicPoly(Eigen::Vector2d(0 ,10), Eigen::Vector2d(0, 0), 3, 1);
//  std::cout << feas_poly;

	test_lsq_4deg();
}



