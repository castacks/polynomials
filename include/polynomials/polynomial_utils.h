/* Copyright 2015 Sanjiban Choudhury
 * polynomial_utils.h
 *
 *  Created on: Mar 4, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef POLYNOMIALS_INCLUDE_POLYNOMIALS_POLYNOMIAL_UTILS_H_
#define POLYNOMIALS_INCLUDE_POLYNOMIALS_POLYNOMIAL_UTILS_H_

#include "polynomials/polynomials.h"

namespace ca {
namespace polynomial_utils {

template <class T, size_t degree_>
T GetMinValue(const Polynomial<T, degree_> &poly) {
  switch (degree_) {
    case 0: // Constant
    {
      return poly[0];
      break;
    }
    case 1: // Linear
    {
      return std::min(poly.Evaluate(poly.break_l()), poly.Evaluate(poly.break_u()));
      break;
    }
    case 2: // Quadratic
    {
      double tau_ext = -0.5 * ( poly[1] / poly[2] );
      if (tau_ext > 0 && tau_ext < poly.GetInterval())
        return std::min(poly[0] - 0.25 * std::pow(poly[1], 2) / poly[2],
                        std::min(poly.Evaluate(poly.break_l()), poly.Evaluate(poly.break_u())));
      else
        return std::min(poly.Evaluate(poly.break_l()), poly.Evaluate(poly.break_u()));

      break;
    }
    default:
    {
      std::vector<T> val;
      for (double alpha = 0; alpha <=1.0; alpha += 0.1)
        val.push_back(poly.Evaluate((1-alpha)*poly.break_l() + alpha*poly.break_u()));
      return *std::min_element(std::begin(val), std::end(val));
    }
  }
}

template <class T, size_t degree_>
T GetMaxValue(const Polynomial<T, degree_> &poly) {
  switch (degree_) {
    case 0: // Constant
    {
      return poly[0];
      break;
    }
    case 1: // Linear
    {
      return std::max(poly.Evaluate(poly.break_l()), poly.Evaluate(poly.break_u()));
      break;
    }
    case 2: // Quadratic
    {
      double tau_ext = -0.5 * ( poly[1] / poly[2] );
      if (tau_ext > 0 && tau_ext < poly.GetInterval())
        return std::max(poly[0] - 0.25 * std::pow(poly[1], 2) / poly[2],
                        std::max(poly.Evaluate(poly.break_l()), poly.Evaluate(poly.break_u())));
      else
        return std::max(poly.Evaluate(poly.break_l()), poly.Evaluate(poly.break_u()));

      break;
    }
    default:
    {
      std::vector<T> val;
      for (double alpha = 0; alpha <=1.0; alpha += 0.1)
        val.push_back(poly.Evaluate((1-alpha)*poly.break_l() + alpha*poly.break_u()));
      return *std::max_element(std::begin(val), std::end(val));
    }
  }
}

template <class T, size_t degree_>
T GetMaxAbsValue(const Polynomial<T, degree_> &poly) {
  switch (degree_) {
    case 0: // Constant
    {
      return poly[0];
      break;
    }
    case 1: // Linear
    {
      return std::max(std::abs(poly.Evaluate(poly.break_l())), std::abs(poly.Evaluate(poly.break_u())));
      break;
    }
    case 2: // Quadratic
    {
      double tau_ext = -0.5 * ( poly[1] / poly[2] );
      if (tau_ext > 0 && tau_ext < poly.GetInterval())
        return std::max(std::abs(poly[0] - 0.25 * std::pow(poly[1], 2) / poly[2]),
                        std::max(std::abs(poly.Evaluate(poly.break_l())), std::abs(poly.Evaluate(poly.break_u()))));
      else
        return std::max(std::abs(poly.Evaluate(poly.break_l())), std::abs(poly.Evaluate(poly.break_u())));
      break;
    }
    default:
    {
      std::vector<T> val;
      for (double alpha = 0; alpha <=1.0; alpha += 0.1)
        val.push_back(poly.Evaluate((1-alpha)*poly.break_l() + alpha*poly.break_u()));
      auto result = std::minmax_element(std::begin(val), std::end(val));
      return std::max(std::abs(*result.first), std::abs(*result.second));
    }
  }
}

}
}

#endif /* POLYNOMIALS_INCLUDE_POLYNOMIALS_POLYNOMIAL_UTILS_H_ */
