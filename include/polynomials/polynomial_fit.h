/* Copyright 2015 Sanjiban Choudhury
 * polynomial_fit.h
 *
 *  Created on: Mar 4, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef POLYNOMIALS_INCLUDE_POLYNOMIALS_POLYNOMIAL_FIT_H_
#define POLYNOMIALS_INCLUDE_POLYNOMIALS_POLYNOMIAL_FIT_H_

#include "polynomials/polynomials.h"
#include <Eigen/Dense>

namespace ca {
namespace polynomial_fit {

typedef struct {
	std::pair<double, double> C0_bv;
	std::pair<double, double> C1_bv;
	std::pair<double, double> C2_bv;
	double limit = 1e9;
	double limit_dot = 1e9;
	double limit_ddot = 1e9;
} PolyfitConstraints;

Polynomial<double, 1> GetLinearPoly(double time_s, double time_f, const Eigen::Vector2d &x);

Polynomial<double, 1> GetFeasInterpLinearPoly(const Eigen::Vector2d &x, double max_xd);

Polynomial<double, 3> GetCubicPoly(double time_s, double time_f, const Eigen::Vector2d &x, const Eigen::Vector2d &xd);

Polynomial<double, 3> GetFeasInterpCubicPoly(const Eigen::Vector2d &x, const Eigen::Vector2d &xd, double max_xd, double max_xdd);

namespace poly5dfit_helper {
const size_t POLY_DEG_5 (5); // degree is 5 (6 coeff)
const size_t NUM_CONSTRAINTS (4);
const size_t SIZE_CHUNK (6);

std::pair<Eigen::MatrixXd,Eigen::VectorXd> GetAeqBeq(
    double t0, double x0, double x0_dot,
    double tT, double xT,  double xT_dot);

std::pair<Eigen::MatrixXd,Eigen::VectorXd> GetAeqBeq(
    double t0, double x0, double x0_dot, double x0_ddot,
    double tT, double xT,  double xT_dot);

std::pair<Eigen::MatrixXd,Eigen::VectorXd> GetQCViaPoint(double time, double val);

std::pair<Eigen::MatrixXd,Eigen::VectorXd> GetQCViaVel(double time, double val);

std::pair<Eigen::MatrixXd,Eigen::VectorXd> GetQCViaAcc(double time, double val);

Eigen::MatrixXd GetQJerk(double time);
}

/**
 * \brief Fit a 5d poly polynoial using pos only
 * @param time_delta
 * @param vals
 * @return Polynomial with REVERSE order (high to low)
 */
Polynomial<double, 5> Fit5DPolyC0 (
  double time_s, double time_f,
  const std::vector<double> &vals);

/**
 * \brief Fit a 5d polynomial using po and vel end constraints and lsq mid constraints
 * @param t0
 * @param x0
 * @param x0_dot
 * @param tT
 * @param xT
 * @param xT_dot
 * @param via_time
 * @param via_val
 * @param weight_lsq
 * @return Polynomial with REVERSE order (high to low)
 */
Polynomial<double,5> Fit5DPolyC1(
    double t0, double x0, double x0_dot,
    double tT, double xT, double xT_dot,
    const std::vector<double> &via_time,
    const std::vector<double> &via_val,
    double weight_lsq);

/**
 * \brief Fit a 5 d polynomial using pos, vel and acc end constraints and lsq mid constraints
 * @param t0
 * @param x0
 * @param x0_dot
 * @param x0_ddot
 * @param tT
 * @param xT
 * @param xT_dot
 * @param via_time
 * @param via_val
 * @param via_dot
 * @param via_ddot
 * @param weight_lsq
 * @param weight_via_vel
 * @param weight_via_acc
 * @return Polynomial with REVERSE order (high to low)
 */
Polynomial<double,5> Fit5DPolyC2(
    double t0, double x0, double x0_dot, double x0_ddot,
    double tT, double xT, double xT_dot,
    const std::vector<double> &via_time,
    const std::vector<double> &via_val,
    const std::vector<double> &via_dot,
    const std::vector<double> &via_ddot,
    double weight_lsq, double weight_via_vel, double weight_via_acc);

/** ---------------------------------- Least squares polynomial fitting ------------------------------------------------------ */

// Populates the C-matrix and d-vector (for (Cx-d))
void Populate_C_d(
	Eigen::MatrixXd &C, Eigen::VectorXd &d,
	const size_t degree, const PolyfitConstraints constraints, const double tf, const size_t num_constraints);

// Populates the A-matrix and b-vector (for (Ax <= b))
void Populate_A_b(
		Eigen::MatrixXd &A, Eigen::VectorXd &b,
		const size_t degree, const PolyfitConstraints constraints, const double tf, const size_t num_samples);

// 3-degree least-squares polynomial fit routine
Polynomial<double, 3> Fit3DPolyC1_lsq(bool &flag, const PolyfitConstraints constraints, const double tf,
		Eigen::VectorXd lb, Eigen::VectorXd ub);

// 4-degree least-squares polynomial fit routine
Polynomial<double, 4> Fit4DPolyC1_lsq(bool &flag, const PolyfitConstraints constraints, const double tf,
		Eigen::VectorXd lb, Eigen::VectorXd ub);

}
}



#endif /* POLYNOMIALS_INCLUDE_POLYNOMIALS_POLYNOMIAL_FIT_H_ */
