/* Copyright 2015 Sanjiban Choudhury
 * polynomials.h
 *
 *  Created on: Mar 4, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef POLYNOMIALS_INCLUDE_POLYNOMIALS_POLYNOMIALS_H_
#define POLYNOMIALS_INCLUDE_POLYNOMIALS_POLYNOMIALS_H_

#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <boost/math/tools/rational.hpp>

namespace ca {

/**
 * \brief A polynomial class
 * stolen from boost/math/tool/polynomial.h .... ups ;-)
 *
 * By default order is from low to high.
 */
template <class T, size_t degree_> class Polynomial {
 public:
  Polynomial(void)
 : coefs_(degree_+1 ,static_cast<T>(0)),
   break_l_(0),
   break_u_(0) {};

  Polynomial(const Polynomial& c_poly) {
    BOOST_ASSERT_MSG(c_poly.Degree() == degree_, "Degree of Polynomial not consistent");
    coefs_ = c_poly.GetCoefs();
    break_l_ = c_poly.break_l();
    break_u_ = c_poly.break_u();
  }

  Polynomial(std::initializer_list<T> l)
  : coefs_(l),
    break_l_(),
    break_u_() {};

  Polynomial(const std::vector<T> &coefs, bool low_to_high=true) {
    BOOST_ASSERT_MSG(coefs.size() == degree_+1, "Number of specified coefficients not consistent with degree of polynomial");
    coefs_ = coefs;// 1. copy coefs
    break_l_ = 0.0;
    break_u_ = 0.0;
    if (!low_to_high) { // different order
      std::reverse(coefs_.begin(),coefs_.end()); //2. reverse order
    }
  };

  Polynomial(const std::vector<T> &coefs, double break_l, double break_u, bool low_to_high=true) {
    BOOST_ASSERT_MSG(coefs.size() == degree_+1, "Number of specified coefficients not consistent with degree of polynomial");
    coefs_ = coefs;// 1. copy coefs
    break_l_ = break_l;
    break_u_ = break_u;
    if (!low_to_high) { // different order
      std::reverse(coefs_.begin(),coefs_.end()); //2. reverse order
    }
  }

  T Evaluate(double val) const {
    //return boost::math::tools::evaluate_polynomial<degree_,T,double>(&coefs_[0], val, degree_+1);
    return boost::math::tools::evaluate_polynomial(&coefs_[0], val - break_l_, degree_+1);
  };

  Polynomial<T,degree_-1> Derivative(void) const {
    BOOST_ASSERT_MSG(degree_>0,"Degree of Polynomial too small to determine derivative");
    std::vector<T> new_coefs(degree_);
    for (ssize_t i(degree_); i > 0; --i) {
      //std::cout << "coefs: " << coefs_[i] << std::endl;
      new_coefs[i - 1] = static_cast<T>(i)* coefs_[i];
      //std::cout << "new coef: " << new_coefs[i-1] << std::endl;
    }
    return Polynomial<T,degree_-1>(new_coefs, break_l_, break_u_);
  };

  void SetToDerivative(const Polynomial<T,degree_+1> poly) {
    BOOST_ASSERT_MSG(degree_+1>0,"Degree of Polynomial too small to determine derivative");
    for (ssize_t i(degree_+1); i > 0; --i) {
      coefs_[i-1] = static_cast<T>(i)* poly[i];
    }
  };

  Polynomial<T,degree_+1> Integral(T constant=0) const {
    std::vector<T> new_coefs(degree_+2);// +2 since +1 for num coefs = degree +1 and +1 for integral
    for (ssize_t i(degree_); i>=0; --i) {
      new_coefs[i+1] = coefs_[i] / static_cast<T>(i+1);
    }
    new_coefs[0] = constant;
    return Polynomial<T,degree_+1>(new_coefs, break_l_, break_u_);
  };

  void SetToIntegral(const Polynomial<T,degree_-1> poly, T constant=0) {
    for (ssize_t i(degree_-1); i>=0; --i) {
      coefs_[i+1] = poly[i] / static_cast<T>(i+1);
    }
    coefs_[0] = constant;
  };

  size_t Degree(void) const { return degree_; };

  size_t Size(void) const { return coefs_.size(); };

  std::vector<T> GetCoefs(void) const { return coefs_; };

  double break_l(void) const { return break_l_; };
  double break_u(void) const { return break_u_; };
  double GetInterval() const { return break_u_ - break_l_; };
  void SetBreaks(double break_l, double break_u) { break_l_ = break_l; break_u_ = break_u;}
  void ResetBreakOffset() { break_u_ -= break_l_; break_l_ = 0;}

  std::vector<T> GetCoefsRev(void) const {
    std::vector<T> tmp(coefs_); // 1. copy coefs
    std::reverse(tmp.begin(),tmp.end()); // 2. make them reverse
    return tmp; // 3. return them
  };

  typename std::vector<T>::const_iterator Begin(void) const { return coefs_.cbegin(); };

  typename std::vector<T>::const_iterator End(void) const   { return coefs_.cend(); };

  T operator[](size_t idx) const {
    BOOST_ASSERT_MSG(idx <= degree_,"Requested coefficient exceed degree of polynomial");
    return coefs_[idx];
  };

  T& operator[](size_t idx) {
    BOOST_ASSERT_MSG(idx <= degree_,"Requested coefficient exceed degree of polynomial");
    return coefs_[idx];
  };

 private:
  /*memebers*/
  std::vector<T> coefs_;
  double break_l_, break_u_;
};

template <class charT, class traits, class T, size_t degree_>
inline std::basic_ostream<charT, traits>& operator << (std::basic_ostream<charT, traits>& os, const Polynomial<T,degree_>& poly) {
  os << "breaks: [" << poly.break_l() << " " << poly.break_u() << "]" << std::endl;
  os << "coefs: [";
  for(size_t i(0); i < poly.Size(); ++i)
  {
    if(i) os << ", ";
    os << poly[i];
  }
  os << "]" << std::endl;
  return os;
}

}//namespace ca




#endif /* POLYNOMIALS_INCLUDE_POLYNOMIALS_POLYNOMIALS_H_ */
