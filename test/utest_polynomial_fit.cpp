/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * utest_polynomial_fit.cpp
 *
 *  Created on: Mar 6, 2016
 *      Author: Sanjiban Choudhury
 */

#include "polynomials/polynomial_fit.h"
// Bring in gtest
#include <gtest/gtest.h>

using namespace ca;
using namespace ca::polynomial_fit;
const double tolerance (1e-8);

TEST(SPLINE_FIT, Polyfit0){
  const std::vector<double> via_time {1,2,3, 6,8};
  const std::vector<double> via_val  {1,5,1,-2,6};
  ca::Polynomial<double,5> p (Fit5DPolyC1(0,0,0,10,10,0,via_time,via_val,1.0));
  std::cout << "Coeffs of Polynomial are: " << p << std::endl;
  /*initial point , end point constraint*/
  EXPECT_DOUBLE_EQ(p.Evaluate(0),  0);
  EXPECT_DOUBLE_EQ(p.Evaluate(10),10);
  /*coeff comparison with MATLAB result*/
  EXPECT_NEAR(p[0], 0.0, tolerance);
  EXPECT_NEAR(p[1], 0.0, tolerance);
  EXPECT_NEAR(p[2], 5.826937735189295e-01, tolerance);
  EXPECT_NEAR(p[3],-2.806281645548255e-01, tolerance);
  EXPECT_NEAR(p[4], 4.364481970539721e-02, tolerance);
  EXPECT_NEAR(p[5],-2.040894098510396e-03, tolerance);
}

TEST(SPLINE_FIT, Polyfit1){
  const std::vector<double> via_time {1,5,10,20,25,30};
  const std::vector<double> via_val  {0,50,600,400,300,450};
  ca::Polynomial<double,5> p (Fit5DPolyC1(-5,-10,5,35,550,-30,via_time,via_val,20.0));
  std::cout << "Coeffs of Polynomial are: " << p << std::endl;

  /*initial point , end point constraint*/
  EXPECT_NEAR(p.Evaluate(-5), -10, tolerance);
  EXPECT_NEAR(p.Evaluate(35), 550, tolerance);
  /*coeff comparison with MATLAB result*/
  EXPECT_NEAR(p[0], -10.000000000000000, tolerance);
  EXPECT_NEAR(p[1], 5.000000000000000 , tolerance);
  EXPECT_NEAR(p[2], 6.594723885137737, tolerance);
  EXPECT_NEAR(p[3], -0.582586872417292, tolerance);
  EXPECT_NEAR(p[4], 0.018014236336271, tolerance);
  EXPECT_NEAR(p[5],-0.000185766048851, tolerance);
}

// Run all the tests that were declared with TEST()
int32_t
main(int32_t argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}



