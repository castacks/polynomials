/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * utest_polynomial.cpp
 *
 *  Created on: Mar 6, 2016
 *      Author: Sanjiban Choudhury
 */

#include "polynomials/polynomials.h"
// Bring in gtest
#include <gtest/gtest.h>

using namespace ca;

TEST(SPLINE_FIT, Polynomial0) {
  Polynomial<double,5> test {1.0,2.0,3.0,4.0,5.0,6.0};
  EXPECT_TRUE(test.Degree() == 5);
  EXPECT_DOUBLE_EQ(test[0],1.0);
  EXPECT_DOUBLE_EQ(test[1],2.0);
  EXPECT_DOUBLE_EQ(test[2],3.0);
  EXPECT_DOUBLE_EQ(test[3],4.0);
  EXPECT_DOUBLE_EQ(test[4],5.0);
  EXPECT_DOUBLE_EQ(test[5],6.0);

  Polynomial<double,4> test_der (test.Derivative());
  EXPECT_TRUE(test_der.Degree() == 4);
  EXPECT_DOUBLE_EQ(test_der[0], 2.0);
  EXPECT_DOUBLE_EQ(test_der[1], 6.0);
  EXPECT_DOUBLE_EQ(test_der[2],12.0);
  EXPECT_DOUBLE_EQ(test_der[3],20.0);
  EXPECT_DOUBLE_EQ(test_der[4],30.0);

  Polynomial<double,3> test_dder (test_der.Derivative());
  EXPECT_TRUE(test_dder.Degree() == 3);
  EXPECT_DOUBLE_EQ(test_dder[0],  6.0);
  EXPECT_DOUBLE_EQ(test_dder[1], 24.0);
  EXPECT_DOUBLE_EQ(test_dder[2], 60.0);
  EXPECT_DOUBLE_EQ(test_dder[3],120.0);

  Polynomial<double,2> test_ddder (test_dder.Derivative());
  EXPECT_TRUE(test_ddder.Degree() == 2);
  EXPECT_DOUBLE_EQ(test_ddder[0], 24.0);
  EXPECT_DOUBLE_EQ(test_ddder[1],120.0);
  EXPECT_DOUBLE_EQ(test_ddder[2],360.0);

  Polynomial<double,1> test_dddder (test_ddder.Derivative());
  EXPECT_TRUE(test_dddder.Degree() == 1);
  EXPECT_DOUBLE_EQ(test_dddder[0],120.0);
  EXPECT_DOUBLE_EQ(test_dddder[1],720.0);
  Polynomial<double,0> test_ddddder (test_dddder.Derivative());
  EXPECT_TRUE(test_ddddder.Degree() == 0);
  EXPECT_DOUBLE_EQ(test_ddddder[0],720.0);

  Polynomial<double,1> test_int (test_ddddder.Integral(120.0));
  EXPECT_TRUE(test_int.Degree() == 1);
  EXPECT_DOUBLE_EQ(test_int[0],120.0);
  EXPECT_DOUBLE_EQ(test_int[1],720.0);

  Polynomial<double,2> test_iint (test_int.Integral(24.0));
  EXPECT_TRUE(test_iint.Degree() == 2);
  EXPECT_DOUBLE_EQ(test_iint[0], 24.0);
  EXPECT_DOUBLE_EQ(test_iint[1],120.0);
  EXPECT_DOUBLE_EQ(test_iint[2],360.0);

  Polynomial<double,3> test_iiint (test_iint.Integral(6.0));
  EXPECT_TRUE(test_iiint.Degree() == 3);
  EXPECT_DOUBLE_EQ(test_iiint[0],  6.0);
  EXPECT_DOUBLE_EQ(test_iiint[1], 24.0);
  EXPECT_DOUBLE_EQ(test_iiint[2], 60.0);
  EXPECT_DOUBLE_EQ(test_iiint[3],120.0);

  Polynomial<double,4> test_iiiint (test_iiint.Integral(2.0));
  EXPECT_TRUE(test_iiiint.Degree() == 4);
  EXPECT_DOUBLE_EQ(test_iiiint[0], 2.0);
  EXPECT_DOUBLE_EQ(test_iiiint[1], 6.0);
  EXPECT_DOUBLE_EQ(test_iiiint[2],12.0);
  EXPECT_DOUBLE_EQ(test_iiiint[3],20.0);
  EXPECT_DOUBLE_EQ(test_iiiint[4],30.0);

  Polynomial<double,5> test_iiiiint (test_iiiint.Integral(1.0));
  EXPECT_TRUE(test_iiiiint.Degree() == 5);
  EXPECT_DOUBLE_EQ(test_iiiiint[0],1.0);
  EXPECT_DOUBLE_EQ(test_iiiiint[1],2.0);
  EXPECT_DOUBLE_EQ(test_iiiiint[2],3.0);
  EXPECT_DOUBLE_EQ(test_iiiiint[3],4.0);
  EXPECT_DOUBLE_EQ(test_iiiiint[4],5.0);
  EXPECT_DOUBLE_EQ(test_iiiiint[5],6.0);

  Polynomial<double,6> test_iiiiiint (test_iiiiint.Integral(-10.0));
  EXPECT_TRUE(test_iiiiiint.Degree() == 6);
  EXPECT_DOUBLE_EQ(test_iiiiiint[0],-10.0);
  EXPECT_DOUBLE_EQ(test_iiiiiint[1],  1.0);
  EXPECT_DOUBLE_EQ(test_iiiiiint[2],  1.0);
  EXPECT_DOUBLE_EQ(test_iiiiiint[3],  1.0);
  EXPECT_DOUBLE_EQ(test_iiiiiint[4],  1.0);
  EXPECT_DOUBLE_EQ(test_iiiiiint[5],  1.0);
  EXPECT_DOUBLE_EQ(test_iiiiiint[6],  1.0);
}

TEST(SPLINE_FIT, Polynomial1) {

  const Polynomial<double,6> test0;
  EXPECT_TRUE(test0.Degree() == 6);
  EXPECT_DOUBLE_EQ(test0[0],0.0);
  EXPECT_DOUBLE_EQ(test0[1],0.0);
  EXPECT_DOUBLE_EQ(test0[2],0.0);
  EXPECT_DOUBLE_EQ(test0[3],0.0);
  EXPECT_DOUBLE_EQ(test0[4],0.0);
  EXPECT_DOUBLE_EQ(test0[5],0.0);


  const Polynomial<float,5> test1 {-5,2,0,10,6,8};
  EXPECT_FLOAT_EQ(test1.GetCoefs()[0],-5);
  EXPECT_FLOAT_EQ(test1.GetCoefs()[1], 2);
  EXPECT_FLOAT_EQ(test1.GetCoefs()[2], 0);
  EXPECT_FLOAT_EQ(test1.GetCoefs()[3],10);
  EXPECT_FLOAT_EQ(test1.GetCoefs()[4], 6);
  EXPECT_FLOAT_EQ(test1.GetCoefs()[5], 8);
  /*now reverse*/
  EXPECT_FLOAT_EQ(test1.GetCoefsRev()[0], 8);
  EXPECT_FLOAT_EQ(test1.GetCoefsRev()[1], 6);
  EXPECT_FLOAT_EQ(test1.GetCoefsRev()[2],10);
  EXPECT_FLOAT_EQ(test1.GetCoefsRev()[3], 0);
  EXPECT_FLOAT_EQ(test1.GetCoefsRev()[4], 2);
  EXPECT_FLOAT_EQ(test1.GetCoefsRev()[5],-5);
  /*some evaluations*/
  EXPECT_FLOAT_EQ(test1.Evaluate(0),-5);
  EXPECT_FLOAT_EQ(test1.Evaluate(1),21);
  EXPECT_FLOAT_EQ(test1.Evaluate(3.5),5532.8750);

  /* initialize with reverse order*/
  std::vector<double> coefs {10,-20,30,40,-50};
  const Polynomial<double,4> test2(coefs,true);
  EXPECT_DOUBLE_EQ(test2[0], 10);
  EXPECT_DOUBLE_EQ(test2[1],-20);
  EXPECT_DOUBLE_EQ(test2[2], 30);
  EXPECT_DOUBLE_EQ(test2[3], 40);
  EXPECT_DOUBLE_EQ(test2[4],-50);
  EXPECT_DOUBLE_EQ(test2.Evaluate(5),-25590.0);

  const Polynomial<double,4> test2_rev(coefs,false);
  EXPECT_DOUBLE_EQ(test2_rev[0],-50);
  EXPECT_DOUBLE_EQ(test2_rev[1], 40);
  EXPECT_DOUBLE_EQ(test2_rev[2], 30);
  EXPECT_DOUBLE_EQ(test2_rev[3],-20);
  EXPECT_DOUBLE_EQ(test2_rev[4], 10);
  EXPECT_DOUBLE_EQ(test2_rev.Evaluate(5),4650.0);

}

// Run all the tests that were declared with TEST()
int32_t
main(int32_t argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}


