/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */

/* Copyright 2015 Sanjiban Choudhury
 * polynomial_utils.cpp
 *
 *  Created on: Mar 4, 2016
 *      Author: Sanjiban Choudhury
 */

#include "polynomials/polynomial_utils.h"
#include <Eigen/Dense>

namespace ca {
namespace polynomial_utils {

}
}

