/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */

/* Copyright 2015 Sanjiban Choudhury
 * polynomial_fit.cpp
 *
 *  Created on: Mar 4, 2016
 *      Author: Sanjiban Choudhury
 */

#include "polynomials/polynomial_fit.h"
#include "polynomials/polynomial_utils.h"
#include "quadprog/quadprog_utils.h"
#include <math.h>
#include "tictoc_profiler/profiler.hpp"

#include <Eigen/Dense>

namespace pu = ca::polynomial_utils;
namespace qp = ca::quadprog_utils;

namespace ca {
namespace polynomial_fit {

Polynomial<double, 1> GetLinearPoly(double time_s, double time_f, const Eigen::Vector2d &x) {
  return Polynomial<double, 1>(std::vector<double>{x[0], (x[1] - x[0])/(time_f - time_s)}, time_s, time_f);
}

Polynomial<double, 1> GetFeasInterpLinearPoly(const Eigen::Vector2d &x, double max_xd) {
  double tau = std::abs(x[0] - x[1]) / max_xd;
  return Polynomial<double, 1>(std::vector<double>{x[0], max_xd}, 0, tau);
}

Polynomial<double, 3> GetCubicPoly(double time_s, double time_f, const Eigen::Vector2d &x, const Eigen::Vector2d &xd) {
  double tau = time_f - time_s;

  Eigen::Matrix4f A;
  A << 1, 0, 0, 0,
       1, tau, std::pow(tau, 2), std::pow(tau, 3),
       0, 1, 0, 0,
       0, 1, 2*tau, 3*std::pow(tau, 2);

  Eigen::Vector4f b;
  b << x[0], x[1], xd[0], xd[1];

  Eigen::Vector4f coeff = A.inverse()*b;
  std::vector<double> coeff_vec(coeff.data(), coeff.data() + coeff.size());
  return Polynomial<double, 3>(coeff_vec, time_s, time_f);
}

Polynomial<double, 3> GetFeasInterpCubicPoly(const Eigen::Vector2d &x, const Eigen::Vector2d &xd, double max_xd, double max_xdd) {
  double a_max = (std::pow(max_xd, 2) - 0.5*( xd.dot(xd) )) / std::abs(x[0] - x[1]);
  double tau_init = (2.0*max_xd - (std::abs(xd[0]) + std::abs(xd[1]))) / a_max;
  for (int i = 1; i <= 10; i++) {
    double tau = tau_init * i;
    Polynomial<double, 3> poly = GetCubicPoly(0, tau, x, xd);
    Polynomial<double, 2> polyd = poly.Derivative();
    Polynomial<double, 1> polydd = polyd.Derivative();
    if (pu::GetMaxAbsValue(polydd) <= max_xdd && pu::GetMaxAbsValue(polyd) <= max_xd)
      return poly;
  }
  return GetCubicPoly(0, 10*tau_init, x, xd);
}


namespace poly5dfit_helper {
/*TODO: put same with fixed size as memebers, initialized in the constructor*/
std::pair<Eigen::MatrixXd,Eigen::VectorXd> GetAeqBeq(double t0, double x0, double x0_dot, double tT, double xT,  double xT_dot) {
  Eigen::MatrixXd Aeq (NUM_CONSTRAINTS,POLY_DEG_5+1);
  Aeq <<
    t0*t0*t0*t0*t0,  t0*t0*t0*t0,  t0*t0*t0,t0*t0, t0, 1,
    5*t0*t0*t0*t0, 4*t0*t0*t0,   3*t0*t0,    2*t0,  1, 0,
    tT*tT*tT*tT*tT,  tT*tT*tT*tT,  tT*tT*tT,tT*tT, tT, 1,
    5*tT*tT*tT*tT, 4*tT*tT*tT,   3*tT*tT,    2*tT,  1, 0;
  Eigen::VectorXd beq (NUM_CONSTRAINTS);
  beq << x0, x0_dot, xT, xT_dot;
  return std::make_pair(Aeq,beq);
}

std::pair<Eigen::MatrixXd,Eigen::VectorXd> GetAeqBeq(double t0, double x0, double x0_dot, double x0_ddot, double tT, double xT,  double xT_dot) {
  Eigen::MatrixXd Aeq (NUM_CONSTRAINTS+1,POLY_DEG_5+1);
  Aeq <<
    t0*t0*t0*t0*t0,  t0*t0*t0*t0,  t0*t0*t0,t0*t0, t0, 1,
    5*t0*t0*t0*t0, 4*t0*t0*t0,   3*t0*t0,    2*t0,  1, 0,
    tT*tT*tT*tT*tT,  tT*tT*tT*tT,  tT*tT*tT,tT*tT, tT, 1,
    5*tT*tT*tT*tT, 4*tT*tT*tT,   3*tT*tT,    2*tT,  1, 0,
        20*t0*t0*t0,  12*t0*t0,      6*t0,       2,     0, 0;// x0_ddot constraint
  Eigen::VectorXd beq (NUM_CONSTRAINTS+1);
  beq << x0, x0_dot, xT, xT_dot, x0_ddot;
  return std::make_pair(Aeq,beq);
}

std::pair<Eigen::MatrixXd,Eigen::VectorXd> GetQCViaPoint(double time, double val) {
  const double t(time);

  Eigen::VectorXd A_tmp(POLY_DEG_5+1);
  A_tmp << t*t*t*t*t, t*t*t*t, t*t*t, t*t, t, 1;
  Eigen::MatrixXd Q (A_tmp*A_tmp.transpose());
  Q = 2*Q;
  Eigen::VectorXd c (-2*val*A_tmp);
  return std::make_pair(Q,c);
}

std::pair<Eigen::MatrixXd,Eigen::VectorXd> GetQCViaVel(double time, double val) {
  const double t(time);
  Eigen::VectorXd A_tmp(POLY_DEG_5+1);
  A_tmp << 5.0*t*t*t*t, 4.0*t*t*t, 3.0*t*t, 2.0*t, 1.0, 0;
  //std::cout << "A_tmp: " << A_tmp << std::endl;
    Eigen::MatrixXd Q (A_tmp*A_tmp.transpose());
  Q = 2*Q;
  Eigen::VectorXd c (-2*val*A_tmp);
  return std::make_pair(Q,c);
}

std::pair<Eigen::MatrixXd,Eigen::VectorXd> GetQCViaAcc(double time, double val) {
  const double t(time);
  Eigen::VectorXd A_tmp(POLY_DEG_5+1);
  A_tmp << 20.0*t*t*t, 12.0*t*t, 6.0*t, 2.0, 0.0, 0.0;
    Eigen::MatrixXd Q (A_tmp*A_tmp.transpose());
  Q = 2*Q;
  Eigen::VectorXd c (-2*val*A_tmp);
  return std::make_pair(Q,c);
}

Eigen::MatrixXd GetQJerk(double time) {
  const double t(time);
  const double t2(t*t);
  const double t3(t2*t);
  const double t4(t3*t);
  const double t5(t4*t);

  Eigen::MatrixXd Q(POLY_DEG_5+1,POLY_DEG_5+1);
  Q <<
    1440*t5,720*t4, 240*t3,0,0,0,
    720*t4, 384*t3, 144*t2,0,0,0,
          240*t3, 144*t2,  72*t, 0,0,0,
      0,      0,      0,   0,0,0,
      0,      0,      0,   0,0,0,
      0,      0,      0,   0,0,0;

  return Q;
}
} // namespace poly5dfit_helper

Polynomial<double,5> Fit5DPolyC0(double time_s, double time_f, const std::vector<double>& vals) {
  using namespace poly5dfit_helper;
  BOOST_ASSERT_MSG(vals.size() == SIZE_CHUNK, "Size of Chunk must be 6");
  std::vector<double> coefs(SIZE_CHUNK);
  double time_delta = time_f - time_s;
  // order low to high degree
  coefs[5] = (-(625.0*(vals[0] - 5.0*vals[1] + 10.0*vals[2] - 10.0*vals[3] + 5.0*vals[4] - vals[5]))/(24.0*(pow(time_delta,5))));
  coefs[4] = ( (625.0*(3.0*vals[0] - 14.0*vals[1] + 26.0*vals[2] - 24.0*vals[3] + 11.0*vals[4] - 2.0*vals[5]))/(24.0*(pow(time_delta,4))));
  coefs[3] = (-(125.0*(17.0*vals[0] - 71.0*vals[1] + 118.0*vals[2] - 98.0*vals[3] + 41.0*vals[4] - 7.0*vals[5]))/(24.0*(pow(time_delta,3))));
  coefs[2] = ( (25.0*(45.0*vals[0] - 154.0*vals[1] + 214.0*vals[2] - 156.0*vals[3] + 61.0*vals[4] - 10.0*vals[5]))/(24.0*(pow(time_delta,2))));
  coefs[1] = (-((137.0*vals[0])/12.0 - 25.0*vals[1] + 25.0*vals[2] - (50.0*vals[3])/3.0 + (25.0*vals[4])/4.0 - vals[5])/time_delta);
  coefs[0] = vals[0];
  return Polynomial<double,POLY_DEG_5> (coefs, time_s, time_f, true); // LOW TO HIGH
}


Polynomial<double,5> Fit5DPolyC1(//Only C1
  double t0, double x0, double x0_dot,
  double tT, double xT, double xT_dot,
  const std::vector<double> &via_time, const std::vector<double> &via_val, double weight_lsq) {
  using namespace poly5dfit_helper;

  Eigen::MatrixXd Q(GetQJerk(tT-t0));
  Eigen::VectorXd c (Eigen::VectorXd::Zero(POLY_DEG_5+1));
  for (size_t i(0); i<via_time.size(); ++i) {
    std::pair<Eigen::MatrixXd,Eigen::VectorXd> QC_via (GetQCViaPoint(via_time[i] - t0, via_val[i]));
    Q += QC_via.first *weight_lsq;
    c += QC_via.second*weight_lsq;
  }
  std::pair<Eigen::MatrixXd,Eigen::VectorXd> AeqBeq (GetAeqBeq(0, x0, x0_dot, tT-t0, xT, xT_dot));

  Eigen::VectorXd coeff_vec;
  qp::SolveQPEqualityCGAL(Q, c, AeqBeq.first, AeqBeq.second, coeff_vec);

  std::vector<double> coefs;
  for (std::size_t i = 0; i < POLY_DEG_5+1; i++)
    coefs.push_back(coeff_vec[i]);
  return Polynomial<double,POLY_DEG_5>(coefs, t0, tT, false);// wrong order
}

// Same as FitPoly, only changed the GetAeqBeq funtion... maybe we should make some small helper functions to avoid copying code...
Polynomial<double,5> Fit5DPolyC2(
  double t0, double x0, double x0_dot, double x0_ddot,
  double tT, double xT, double xT_dot,
  const std::vector<double> &via_time, const std::vector<double> &via_val, const std::vector<double> &via_dot, const std::vector<double> &via_ddot, double weight_lsq, double weight_via_vel, double weight_via_acc) {
  using namespace poly5dfit_helper;

  //Eigen::MatrixXd Q(GetQJerk(tT-t0));
  //Eigen::VectorXd c (Eigen::VectorXd::Zero(POLY_DEG_5+1));
  Eigen::MatrixXd Q(Eigen::MatrixXd::Zero(POLY_DEG_5+1,POLY_DEG_5+1));
    Eigen::VectorXd c(Eigen::VectorXd::Zero(POLY_DEG_5+1));
    // for via points
    if (weight_lsq > 0) {
        for (size_t i(0); i<via_time.size(); ++i) {
            std::pair<Eigen::MatrixXd,Eigen::VectorXd> QC_via (GetQCViaPoint(via_time[i]-t0, via_val[i]));
            Q += QC_via.first *weight_lsq;
            c += QC_via.second*weight_lsq;
        }
    }
  // for via vels
    if (weight_via_vel > 0) {
        for (size_t i(0); i<via_time.size(); ++i) {
            std::pair<Eigen::MatrixXd,Eigen::VectorXd> QC_via (GetQCViaVel(via_time[i]-t0, via_dot[i]));
            Q += QC_via.first *weight_via_vel;
            c += QC_via.second*weight_via_vel;

        }
    }
  // for via acc
    if (weight_via_acc > 0 ) {
        for (size_t i(0); i<via_time.size(); ++i) {
            std::pair<Eigen::MatrixXd,Eigen::VectorXd> QC_via (GetQCViaAcc(via_time[i]-t0, via_ddot[i]));
            Q += QC_via.first *weight_via_acc;
            c += QC_via.second*weight_via_acc;

        }
    }
    //std::cout << "Q: \n" << Q << std::endl;
  std::pair<Eigen::MatrixXd,Eigen::VectorXd> AeqBeq (GetAeqBeq(0, x0, x0_dot, x0_ddot, tT-t0, xT, xT_dot));

  Eigen::VectorXd coeff_vec;
  qp::SolveQPEqualityCGAL(Q, c, AeqBeq.first, AeqBeq.second, coeff_vec);

  std::vector<double> coefs;
  for (std::size_t i = 0; i < POLY_DEG_5+1; i++)
    coefs.push_back(coeff_vec[i]);

  return Polynomial<double,POLY_DEG_5>(coefs, t0, tT, false);// wrong order
}

/** ----------------------------- Least squares polynomial fitting ------------------------------------ */

/*
 * Minimizes ||Cx-D||^2 while respecting Ax <= b.
 */
Polynomial<double, 3> Fit3DPolyC1_lsq(bool &flag, const PolyfitConstraints constraints, const double tf,
		Eigen::VectorXd lb, Eigen::VectorXd ub) {
	int degree = 3;
	int num_rows_cost = 4;

	double err_tol = 1e-8;
	int num_samples = 100;

	// Set up cost function matrices (Cx - d)
	Eigen::MatrixXd C(num_rows_cost, degree+1);
	Eigen::VectorXd d(num_rows_cost);
	Profiler::tictoc("Populate C_d");
	Populate_C_d(C, d, degree, constraints, tf, num_rows_cost);
	Profiler::tictoc("Populate C_d");

	// Set up constraints matrix and vector (Ax <= b)
	Eigen::MatrixXd A(6*num_samples, degree+1);
	Eigen::VectorXd b(6*num_samples);
	Profiler::tictoc("Populate A_b");
	Populate_A_b(A, b, degree, constraints, tf, num_samples);
	Profiler::tictoc("Populate A_b");

	// Set up matrices for quadprog
	Eigen::MatrixXd Q = (C.transpose() * C);
	Eigen::VectorXd c_qp = -1 * C.transpose() * d;

	// Get QP solution
	Eigen::VectorXd X(degree+1);
	Profiler::tictoc("Solve QP");
	flag = qp::SolveQPInequalityCGAL(Q, c_qp, A, b, lb, ub, X);
	Profiler::tictoc("Solve QP");

	Profiler::tictoc("Check quality");
	flag = qp::CheckLSQSolutionQuality(C, d, A, b, X, 1e-8, 1e-8);
	Profiler::tictoc("Check quality");

	ca::Profiler::print_aggregated(std::cerr);

	std::vector<double> coeffs(X.data(), X.data() + X.size());
	return Polynomial<double, 3>(coeffs, 0, tf, false);
}

/*
 * Minimizes ||Cx-D||^2 while respecting Ax <= b.
 */
Polynomial<double, 4> Fit4DPolyC1_lsq(bool &flag, const PolyfitConstraints constraints, const double tf,
		Eigen::VectorXd lb, Eigen::VectorXd ub) {
	int degree = 4;
	int num_rows_cost = 4;

	double err_tol = 1e-8;
	int num_samples = 100;

//	Profiler::enable();

	// Set up cost function matrices (Cx - d)
//	std::cout << "Getting C and d" << "\n";
	Eigen::MatrixXd C(num_rows_cost, degree+1);
	Eigen::VectorXd d(num_rows_cost);
//	Profiler::tictoc("Populate C_d");
	Populate_C_d(C, d, degree, constraints, tf, num_rows_cost);
//	Profiler::tictoc("Populate C_d");
//	std::cout << "Got C and d" << "\n";

	// Set up constraints matrix and vector (Ax <= b)
//	std::cout << "Getting A and b" << "\n";
	Eigen::MatrixXd A = Eigen::MatrixXd::Zero(6*num_samples, degree+1);
	Eigen::VectorXd b = Eigen::VectorXd::Zero(6*num_samples);
//	Profiler::tictoc("Populate A_b");
	Populate_A_b(A, b, degree, constraints, tf, num_samples);
//	Profiler::tictoc("Populate A_b");
//	std::cout << "Got A and b" << "\n";


	// Set up matrices for quadprog
	Eigen::MatrixXd Q = (C.transpose() * C);
	Eigen::VectorXd c_qp = -1 * C.transpose() * d;

	// Get QP solution
	Eigen::VectorXd X(degree+1);
//	std::cout << "Getting QP" << "\n";
//	Profiler::tictoc("Solve QP");
	flag = qp::SolveQPInequalityCGAL(Q, c_qp, A, b, lb, ub, X);
//	Profiler::tictoc("Solve QP");
//	Profiler::tictoc("Check quality");
	flag = qp::CheckLSQSolutionQuality(C, d, A, b, X, 1e-8, 1e-8);
//	Profiler::tictoc("Check quality");
//	std::cout << "Got QP" << "\n";

//	// Write matrices to file
//	char fn[50];
//	FILE *out_file;
//	sprintf(fn, "C.txt");
//	out_file = fopen(fn, "w");
//	for(size_t i = 0; i < num_rows_cost; ++i) {
//		fprintf(out_file, "%lf,%lf,%lf,%lf,%lf\n", C(i,0), C(i,1), C(i,2), C(i,3), C(i,4));
//	}
//	fclose(out_file);
//	sprintf(fn, "A.txt");
//	out_file = fopen(fn, "w");
//	for(size_t i = 0; i < 6*num_samples; ++i) {
//		fprintf(out_file, "%lf,%lf,%lf,%lf,%lf\n", A(i,0), A(i,1), A(i,2), A(i,3), A(i,4));
//	}
//	fclose(out_file);
//	sprintf(fn, "d.txt");
//	out_file = fopen(fn, "w");
//	fprintf(out_file, "%lf,%lf,%lf,%lf", d(0), d(1), d(2), d(3));
//	fclose(out_file);
//	sprintf(fn, "b.txt");
//	out_file = fopen(fn, "w");
//	for(size_t i = 0; i < 6*num_samples; ++i) {
//		fprintf(out_file, "%lf\n", b(i));
//	}
//	fclose(out_file);

//	ca::Profiler::print_aggregated(std::cout);

	std::vector<double> coeffs(X.data(), X.data() + X.size());
	return Polynomial<double, 4>(coeffs, 0, tf, false);
}

// Populates the C-matrix and d-vector (for (Cx-d))
void Populate_C_d(
	Eigen::MatrixXd &C, Eigen::VectorXd &d,
	const size_t degree, const PolyfitConstraints constraints, const double tf, const size_t num_constraints) {

//	std::cout << "In C+d: r-" << C.rows() << ", c-" << C.cols() << "\n";
	// Populate first two rows of C, for C0.
	for(int i = degree; i >= 0; --i) {
//		std::cout << i << "\n";
		C(0, degree-i) = std::pow(0, i);
		C(1, degree-i) = std::pow(tf, i);
	}
	d(0) = constraints.C0_bv.first;
	d(1) = constraints.C0_bv.second;
//	std::cout << "In C+d. Got first 2 rows" << "\n";

	// Populate next two rows, for C1
	if(num_constraints > 2) {
		for(int i = degree; i >= 1; --i) {
			C(2, degree-i) = i * std::pow(0, i-1);
			C(3, degree-i) = i * std::pow(tf, i-1);
		}
		C(2, degree) = 0;
		C(3, degree) = 0;

		d(2) = constraints.C1_bv.first;
		d(3) = constraints.C1_bv.second;
	}
//	std::cout << "In C+d. Got last 2 rows" << "\n";
}

// Populates the A-matrix and b-vector (for (Ax <= b))
void Populate_A_b(
		Eigen::MatrixXd &A, Eigen::VectorXd &b,
		const size_t degree, const PolyfitConstraints constraints, const double tf, const size_t num_samples) {

	// Generate samples between 0 and tf
	Eigen::VectorXd samples = Eigen::VectorXd::LinSpaced(Eigen::Sequential, num_samples, 0, tf);

//	std::cout << A.rows() << ", " << A.cols() << "; " << b.rows() << ", " << b.cols() << "\n";

	// Set up value constraints
	int start_idx = 0;
//	std::cout << "Start idx " << start_idx << "\n";
	for(int i = start_idx; i < start_idx + num_samples; ++i) {
		for(int j = degree; j >= 0; --j) {
			A(i, degree-j) = std::pow(samples(i-start_idx), j);
			A(i+num_samples, degree-j) = -std::pow(samples(i-start_idx), j);
		}
	}
	b.segment(start_idx, 2*num_samples) += Eigen::VectorXd::Constant(2*num_samples, constraints.limit);

	// Set up rate constraints
	start_idx = 2*num_samples;
//	std::cout << "Start idx " << start_idx << "\n";
	for(int i = start_idx; i < start_idx + num_samples; ++i) {
		for(int j = degree; j >= 1; --j) {
//			std::cout << i << ", " <<  j << "\n";
			A(i, degree-j) = j * std::pow(samples(i-start_idx), j-1);
			A(i+num_samples, degree-j) = -j * std::pow(samples(i-start_idx), j-1);
		}
	}
//	std::cout << "Got A2. " << start_idx+2*num_samples << "\n";
	b.segment(start_idx, 2*num_samples) += Eigen::VectorXd::Constant(2*num_samples, constraints.limit_dot);

	// Set up rate-rate constraints
	start_idx = 4*num_samples;
//	std::cout << "Start idx " << start_idx << "\n";
	for(int i = start_idx; i < start_idx + num_samples; ++i) {
		for(int j = degree; j >= 2; --j) {
			A(i, degree-j) = j * (j-1) * std::pow(samples(i-start_idx), j-2);
			A(i+num_samples, degree-j) = -j * (j-1) * std::pow(samples(i-start_idx), j-2);
		}
	}
	b.segment(start_idx, 2*num_samples) += Eigen::VectorXd::Constant(2*num_samples, constraints.limit_ddot);
}

}
}


